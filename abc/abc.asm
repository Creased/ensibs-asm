.386
.model flat,stdcall
option casemap:none

include D:\bin\masm32\include\windows.inc
include D:\bin\masm32\include\kernel32.inc
include D:\bin\masm32\include\msvcrt.inc

includelib D:\bin\masm32\lib\kernel32.lib
includelib D:\bin\masm32\lib\msvcrt.lib

.DATA
    userInputPrompt   db "Your text #> ", 0
    abcPrompt         db "a=%d b=%d c=%d", 10, 0
    scanfFormat       db "%s", 0

.DATA?
     userText db ?

.CODE
    abc_count PROC
        ; création d'une nouvelle stack frame
        push ebp          ; place le bas de pile (ebp) en haut de pile (esp)
        mov ebp, esp      ; place la valeur de esp dans ebp

        sub esp, 12       ; prépare de la place sur la stack pour stocker 3 entiers
                          ; de 32 bits (DWORD)

        xor ebx, ebx
        mov [ebp-4], ebx   ; location of variable a
        mov [ebp-8], ebx   ; location of variable b
        mov [ebp-12], ebx  ; location of variable c

        mov esi, [ebp+8]  ; récupère la chaîne de caractères à traiter
                          ; depuis la stack et la place dans le registre esi

        for_loop:
            mov al, [esi] ; récupère un caractère de la chaîne et le place dans al
            mov ebx, 1    ; valeur utilisée pour l'incrémentation des compteur a, b et c

            test_a:
                ; al == 'a'
                cmp al, 97
                jne test_b
                add [ebp-4], ebx
                jmp next

            test_b:
                ; al == 'b'
                cmp al, 98
                jne test_c
                add [ebp-8], ebx
                jmp next

            test_c:
                ; al == 'c'
                cmp al, 99
                jne next
                add [ebp-12], ebx
                jmp next

        next:
            inc esi       ; incrémente notre position dans le registre ESI (passage au caractère suivant)
            test al, al   ; vérifie qu'il reste un caractère à traiter
            jnz for_loop  ; si il reste un caractère, on retourne dans la boucle pour le passer en majuscule

        return:
            mov eax, [ebp-4]
            mov ebx, [ebp-8]
            mov ecx, [ebp-12]

            mov esp, ebp      ; retour sur l'ancienne stackframe
            pop ebp           ; place la valeur contenue dans ebp sur la stack (adresse de retour)
            ret               ; retour à l'adresse indiquée sur la stack (précédemment dans ebp)
    abc_count ENDP

    start:
        push offset userInputPrompt
        call crt_printf ; Affiche le prompt pour la saisie utilisateur
                        ; équivalent en C : printf("Your text #> ");

        push offset userText
        push offset scanfFormat
        call crt_scanf ; récupère la saisie utilisateur depuis l'entrée standard
                       ; et la stocke dans nValue
                       ; équivalent en C : scanf("%s", &userText);

        push offset userText
        call abc_count ; appel de la fonction uppercase avec la saisie de l'utilisateur en paramètre

        push ecx
        push ebx
        push eax
        push offset abcPrompt
        call crt_printf ; affiche la saisie utilisateur

        mov eax, 0
        invoke ExitProcess, eax ; met fin à l'exécution du programme
                                ; et retourne un code d'erreur à 0
                                ; équivalent en C : exit(0);
    end start
