.386
.model flat,stdcall
option casemap:none

include D:\bin\masm32\include\windows.inc
include D:\bin\masm32\include\kernel32.inc
include D:\bin\masm32\include\msvcrt.inc

includelib D:\bin\masm32\lib\kernel32.lib
includelib D:\bin\masm32\lib\msvcrt.lib

.DATA
    userInputPrompt   db "Your n value #> ", 0
    mystPrompt        db "myst function result: %d", 10, 0
    scanfFormat       db "%d", 0

.DATA?
    nValue dword ?

.CODE
    myst PROC ; Suite de Fibonacci
        ; création d'une nouvelle stack frame
        push ebp          ; place le bas de pile (ebp) en haut de pile (esp)
        mov ebp, esp      ; place la valeur de esp dans ebp

        sub esp, 16       ; prépare de la place sur la stack pour stocker 4 entiers
                          ; de 32 bits (DWORD)

        mov ebx, 3
        mov [ebp-4], ebx   ; location of variable i
        mov ebx, 1
        mov [ebp-8], ebx   ; location of variable j
        mov ebx, 1
        mov [ebp-12], ebx  ; location of variable k
        xor ebx, ebx
        mov [ebp-16], ebx  ; location of variable l

        mov ecx, [ebp-4]   ; move i to ecx
        ; for (i=3; i <= n; i++)
        for_loop:
            ; l = j + k
            mov ebx, [ebp-8]
            add ebx, [ebp-12]
            mov [ebp-16], ebx

            ; j = k
            mov ebx, [ebp-12]
            mov [ebp-8], ebx

            ; k = l
            mov ebx, [ebp-16]
            mov [ebp-12], ebx

            ; i++
            ; mov ebx, [ebp-4]
            ; inc ebx
            ; mov [ebp-4], ebx
            inc ecx

            ; compare i à n et fait un branchement vers la boucle si i > n
            mov eax, [ebp+8]
            cmp ecx, eax
            jle for_loop

        return:
            mov eax, [ebp-12] ; retourne k
            mov esp, ebp      ; retour sur l'ancienne stackframe
            pop ebp           ; place la valeur contenue dans ebp sur la stack (adresse de retour)
            ret               ; retour à l'adresse indiquée sur la stack (précédemment dans ebp)
    myst ENDP

    start:
        push offset userInputPrompt
        call crt_printf ; Affiche le prompt pour la saisie utilisateur
                        ; équivalent en C : printf("Your n value #> ");

        push offset nValue
        push offset scanfFormat
        call crt_scanf ; récupère la saisie utilisateur depuis l'entrée standard
                       ; et la stocke dans nValue
                       ; équivalent en C : scanf("%s", &nValue);

        push dword ptr nValue
        call myst ; appel de la fonction uppercase avec la saisie de l'utilisateur en paramètre

        push eax
        push offset mystPrompt
        call crt_printf ; affiche la saisie utilisateur

        mov eax, 0
        invoke ExitProcess, eax ; met fin à l'exécution du programme
                                ; et retourne un code d'erreur à 0
                                ; équivalent en C : exit(0);
    end start
