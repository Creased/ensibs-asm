#include <stdio.h>

int myst(int n) {
  int i, j, k, l;
  j = 1;
  k = 1;
  for (i=3; i<=n; i++) {
    l = j + k;
    j = k;
    k = l;
  }
  return k;
}


int main(int argc, char *argv[]) {
  int nValue, mystResult;

  printf("Your n value #> ");
  scanf("%d", &nValue);
  mystResult = myst(nValue);
  printf("myst function result: %d\n", mystResult);

  return 0;
}
