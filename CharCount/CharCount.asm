.386
.model flat,stdcall
option casemap:none

include D:\bin\masm32\include\windows.inc
include D:\bin\masm32\include\kernel32.inc
include D:\bin\masm32\include\msvcrt.inc

includelib D:\bin\masm32\lib\kernel32.lib
includelib D:\bin\masm32\lib\msvcrt.lib

.DATA
    charCountPrompt   db "Char count: %d", 10, 0
    userInputPrompt   db "Your text #> ", 0
    scanfFormat       db "%s", 0

.DATA?
    userText  db ?

.CODE
    charcount PROC
        ; création d'une nouvelle stack frame
        push ebp          ; place le bas de pile (ebp) en haut de pile (esp)
        mov ebp, esp      ; place la valeur de esp dans ebp

        xor eax, eax       ; mise à zero du registre eax
        mov edi, [esp+8]   ; récupère la chaîne de caractères à traiter
                           ; depuis la stack et la place dans le registre edi
        mov ebx, edi       ; sauvegarde du pointeur de chaîne dans le registre ebx

        repne scasb        ; itération sur al des octets de la chaîne pointée par edi
                           ; jusqu'à trouver un NUL byte correspondant à la fin de la
                           ; chaîne

        sub edi, ebx       ; on calcule l'offset correspondant à l'écart entre le pointeur
                           ; du dernier caractère et celui du début de chaîne pour obtenir
                           ; la longueur totale de la chaîne (\n inclus !)

        return:
            mov eax, edi
            mov esp, ebp  ; retour sur l'ancienne stackframe
            pop ebp       ; place la valeur contenue dans ebp sur la stack (adresse de retour)
            ret           ; retour à l'adresse indiquée sur la stack (précédemment dans ebp)
    charcount ENDP

    start:
        push offset userInputPrompt
        call crt_printf ; Affiche le prompt pour la saisie utilisateur
                        ; équivalent en C : printf("Your text #> ");

        push offset userText
        push offset scanfFormat
        call crt_scanf ; récupère la saisie utilisateur depuis l'entrée standard
                       ; et la stocke dans userText
                       ; équivalent en C : scanf("%s", &userText);

        push offset userText
        call charcount ; appel de la fonction uppercase avec la saisie de l'utilisateur en paramètre

        push eax
        push offset charCountPrompt
        call crt_printf ; affiche le nombre de caractères dans la chaîne saisie par l'utilisateur

        mov eax, 0
        invoke ExitProcess, eax ; met fin à l'exécution du programme
                                ; et retourne un code d'erreur à 0
                                ; équivalent en C : exit(0);
    end start
