.386
.model flat,stdcall
option casemap:none

include D:\bin\masm32\include\windows.inc
include D:\bin\masm32\include\kernel32.inc
include D:\bin\masm32\include\msvcrt.inc

includelib D:\bin\masm32\lib\kernel32.lib
includelib D:\bin\masm32\lib\msvcrt.lib

.DATA
    userInputPrompt   db "Your n value #> ", 0
    diviseurPrompt    db "Un diviseur de %d : %d", 10, 0
    scanfFormat       db "%d", 0

.DATA?
     nValue dword ?

.CODE
    diviseur PROC
        ; création d'une nouvelle stack frame
        push ebp          ; place le bas de pile (ebp) en haut de pile (esp)
        mov ebp, esp      ; place la valeur de esp dans ebp

        sub esp, 4        ; prépare de la place sur la stack pour stocker un entier de 32 bits (DWORD)

        mov ecx, 1 ; initialisation du diviseur à 1
        for_loop:
            mov ebx, ecx  ; place la valeur incrémentée de ecx (notre diviseur) dans ebx

            mov eax, [ebp+8]  ; récupère la valeur saisie par l'utilisateur et la
                              ; place dans eax
            xor edx, edx      ; vide le dividende
            mov [ebp-4], ebx  ; sauvegarde ebx sur la stack avant l'appel à printf
            div ebx           ; divise eax par ebx et stocke la valeur dans edx
            test edx, edx     ; définit ZF à 1 si edx = 0
            jne next

        print_diviseur:
            push ecx
            push [ebp+8]
            push offset diviseurPrompt
            call crt_printf

        next:
            mov ecx, [ebp-4] ; récupère la valeur de ecx sauvegardée sur la stack
            inc ecx          ; incrémente notre diviseur
            cmp ecx, [ebp+8] ; vérifie qu'on a pas encore atteint la valeur saisie par l'utilisateur
            jle for_loop     ; si il reste des itération, on retourne dans la boucle

        return:
            mov esp, ebp      ; retour sur l'ancienne stackframe
            pop ebp           ; place la valeur contenue dans ebp sur la stack (adresse de retour)
            ret               ; retour à l'adresse indiquée sur la stack (précédemment dans ebp)
    diviseur ENDP

    start:
        push offset userInputPrompt
        call crt_printf ; Affiche le prompt pour la saisie utilisateur
                        ; équivalent en C : printf("Your n value #> ");

        push offset nValue
        push offset scanfFormat
        call crt_scanf ; récupère la saisie utilisateur depuis l'entrée standard
                       ; et la stocke dans nValue
                       ; équivalent en C : scanf("%d", &userNumber);

        push dword ptr nValue
        call diviseur ; appel de la fonction diviseur avec la saisie de l'utilisateur en paramètre

        mov eax, 0
        invoke ExitProcess, eax ; met fin à l'exécution du programme
                                ; et retourne un code d'erreur à 0
                                ; équivalent en C : exit(0);
    end start
