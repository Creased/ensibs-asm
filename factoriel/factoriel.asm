.386
.model flat,stdcall
option casemap:none

include D:\bin\masm32\include\windows.inc
include D:\bin\masm32\include\kernel32.inc
include D:\bin\masm32\include\msvcrt.inc

includelib D:\bin\masm32\lib\kernel32.lib
includelib D:\bin\masm32\lib\msvcrt.lib

.DATA
    userInputPrompt   db "Your n value #> ", 0
    factorielPrompt    db "Factoriel de %d : %d", 10, 0
    scanfFormat       db "%d", 0

.DATA?
     nValue dword ?

.CODE
    factoriel PROC
        ; création d'une nouvelle stack frame
        push ebp          ; place le bas de pile (ebp) en haut de pile (esp)
        mov ebp, esp      ; place la valeur de esp dans ebp

        mov ebx, [esp+8]
        cmp ebx, 1
        jne calc_factoriel
        mov eax, 1
        jmp return

        calc_factoriel:
            dec ebx
            push ebx
            call factoriel
            xor edx, edx
            mov ebx, [ebp+8]
            mul ebx

        return:
            mov esp, ebp      ; retour sur l'ancienne stackframe
            pop ebp           ; place la valeur contenue dans ebp sur la stack (adresse de retour)
            ret               ; retour à l'adresse indiquée sur la stack (précédemment dans ebp)
    factoriel ENDP

    start:
        push offset userInputPrompt
        call crt_printf ; Affiche le prompt pour la saisie utilisateur
                        ; équivalent en C : printf("Your n value #> ");

        push offset nValue
        push offset scanfFormat
        call crt_scanf ; récupère la saisie utilisateur depuis l'entrée standard
                       ; et la stocke dans nValue
                       ; équivalent en C : scanf("%d", &userNumber);

        push dword ptr nValue
        call factoriel ; appel de la fonction factoriel avec la saisie de l'utilisateur en paramètre

        push eax
        push nValue
        push offset factorielPrompt
        call crt_printf ; affiche la saisie utilisateur

        mov eax, 0
        invoke ExitProcess, eax ; met fin à l'exécution du programme
                                ; et retourne un code d'erreur à 0
                                ; équivalent en C : exit(0);
    end start
