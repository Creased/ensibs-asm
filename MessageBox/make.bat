@echo off

REM /c Assemble without linking
REM /Zd Add line number debug info
REM /coff generate COFF format object file

D:\bin\masm32\bin\ml /c /Zd /coff MessageBox.asm
D:\bin\masm32\bin\Link /SUBSYSTEM:CONSOLE MessageBox.obj

pause
