.386
.model flat,stdcall
option casemap:none

include D:\bin\masm32\include\windows.inc
include D:\bin\masm32\include\user32.inc
include D:\bin\masm32\include\kernel32.inc

includelib D:\bin\masm32\lib\kernel32.lib
includelib D:\bin\masm32\lib\user32.lib

.DATA
    MsgBoxText    db "Hello, World!", 0
    MsgBoxCaption db "Hello", 0

.DATA?

.CODE
    start:
        push MB_ICONINFORMATION
        push offset MsgBoxCaption
        push offset MsgBoxText
        push NULL
        call MessageBox

        mov eax, 0
        invoke ExitProcess, eax
    end start
