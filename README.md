# Assembleur x86 - TP1

## Compilation

La compilation nécessite l'utilisation d'un assembleur et un éditeur de liens,
pour cela, nous pouvons utiliser l'assembleur masm32&nbsp;:

```dos
@echo off

REM /c Assemble without linking
REM /Zd Add line number debug info
REM /coff generate COFF format object file

D:\bin\masm32\bin\ml /c /Zd /coff HelloWorld.asm
D:\bin\masm32\bin\Link /SUBSYSTEM:CONSOLE HelloWorld.obj

pause
```

La première étape consiste essentiellement à compiler l'assembly en objet
(ELF ou PE en fonction du système exploité), la deuxième à créer les liens
entre l'objet et les différentes bibliothèques.

## Debugging

Pour le debugging de notre programme, il existe de nombreux debugger,
personnellement j'utilise [Immunity Debugger](https://www.immunityinc.com/products/debugger/)
ou [x64dbg](https://x64dbg.com/) qui ont tous deux l'avantage d'être
open-source.

## Appel de fonction

Une fois Immunity Debugger installé, il suffit de charger le programme assemblé
précédemment pour observer les différentes instructions décrites   dans notre
fichier [HellWorld.asm](HellWorld/HelloWorld.asm).

![debugger_hello_world][]

### Étude de l'appel de fonction `crt_printf`

Pour afficher du texte sur la sortie standard, on utilise la fonction `crt_printf`.
Pour appeler cette fonction, on va commencer par *push* les arguments à passer à la fonction sur la *stack*&nbsp;:

```asm
00401000 >/$ 6A 2A          PUSH 2A                                  ; /<%d> = 2A (42.)
00401002  |. 68 00304000    PUSH HelloWor.00403000                   ; |format = "Hello World : %d\n"
```

L'appel final souhaité est `printf("Hello World : %d\n", 42);`, on *push* donc tout d'abord `42` puis `"Hello World : %d\n"` sur la *stack*.

L'appel de fonction peut ensuite être fait simplement en utilisant l'instruction `call`&nbsp;:

```asm
00401007  |. FF15 0C204000  CALL DWORD PTR DS:[<&msvcrt.printf>]     ; \printf
```

Suite à l'exécution de ces trois instructions, on peut constater que les données
sont bien présentes sur la *stack* et que l'appel de fonction est bien inscrit
sur la *stack*.

Une quatrième valeur est également placée sur la *stack*, il s'agit
de l'adresse de retour de la fonction. Cette adresse permet simplement de décrire
l'adresse où se trouvent les prochaines instructions à exécuter après l'exécution de
la fonction `printf`&nbsp;:

![debugger_hello_world_call_printf][]

### Exemple d'utilisation de l'API Windows avec MessageBox

Pour utiliser la fonction `MessageBox`, on peut trouver sa documentation sur le [MSDN](https://msdn.microsoft.com/en-us/library/windows/desktop/ms645505.aspx)&nbsp;:

```cpp
int WINAPI MessageBox(
  _In_opt_ HWND    hWnd,       // handle to owner window
  _In_opt_ LPCTSTR lpText,     // text in message box
  _In_opt_ LPCTSTR lpCaption,  // message box title
  _In_     UINT    uType       // message box style
);
```

Le premier argument correspond à l'identifiant de la fenêtre propriétaire de
cette boîte de dialogue, ici il n'y en a pas puisqu'il s'agit d'un programme basé
sur une console. La documentation indique la possibilité de passer `NULL` comme
valeur pour indiquer que la boîte de dialogue n'est attachée à aucune fenêtre.

Ici, on souhaite procéder à l'appel suivant&nbsp;:

```cpp
MessageBox(NULL, "Hello, World!", "Hello", MB_ICONINFORMATION);
```

Le code assembleur correspondant est donc&nbsp;:

```asm
.386
.model flat,stdcall
option casemap:none

include D:\bin\masm32\include\windows.inc
include D:\bin\masm32\include\user32.inc
include D:\bin\masm32\include\kernel32.inc

includelib D:\bin\masm32\lib\kernel32.lib
includelib D:\bin\masm32\lib\user32.lib

.DATA
		MsgBoxText    db "Hello, World!", 0
		MsgBoxCaption db "Hello", 0

.DATA?

.CODE
		start:
		    push MB_ICONINFORMATION
		    push offset MsgBoxCaption
		    push offset MsgBoxText
		    push NULL
		    call MessageBox

		    mov eax, 0
		    invoke ExitProcess, eax
		end start
```

Explications&nbsp;:

 - `.386`&nbsp;: on déclare un assembly d'instructions pour une architecture
basée sur un processeur [80386](https://en.wikipedia.org/wiki/Intel_80386)
 - `.model`&nbsp;: initialise le modèle de mémoire du programme permettant de
définir la taille des pointeurs de données et du code ainsi que les conventions
d'appels et de nommage des instructions
 - `option casemap:none`&nbsp;: permet d'indiquer à notre assembleur que la casse
n'est pas punitive
 - `include blah`&nbsp;: indique à l'assembleur d'inclure le code source du fichier `blah`
dans notre propre code
 - `includelib blah`&nbsp;: indique à l'éditeur de liens de lier le module `blah` à notre assembly
 - `.DATA`&nbsp;: créer un segment pour le stockage de données
 - `.DATA?`&nbsp;: créer un segment pour le stockage de données non initialisées
 - `.CODE`&nbsp;: créer un segment contenant le code de notre programme
 - `start` […] `end start`&nbsp;: décrit le code source, notre routine start

Finalement, le code contenu dans la routine `start` ne permet ni plus ni moins
que de procéder aux appels de fonction suivant&nbsp;:

```cpp
MessageBox(NULL, "Hello, World!", "Hello", MB_ICONINFORMATION);
ExitProcess(0);
```

## Modes d'adressage

### Création d'un sous-programme

Un sous-programme (ou fonction) est un sous-ensemble d'un programme permettant
tout comme le programme principal d'être appelé à de multiples reprises avec
des arguments.

La déclaration d'un sous programme se fait comme suit&nbsp;:

```asm
.CODE
  function PROC
      ...
  function ENDP

  start:
    push ...
    call function ; appel de la fonction avec un ou plusieurs arguments (sur la stack)
  end start
```

Ici les arguments sont passés par la pile, pour cela on va *push* la valeur à passer
à la fonction sur la stack et procéder à l'appel de fonction.

Afin de garantir une exécution normale et fiable de la routine, nous allons initier
une nouvelle *stackframe*&nbsp;:

```asm
push ebp          ; place le bas de pile (ebp) en haut de pile (esp)
mov ebp, esp      ; place la valeur de esp dans ebp
```

Pour l'exemple, nous allons créer un sous-programme permettant de passer en
majuscule une chaîne de caractère saisie par l'utilisateur dans le corps du programme.

Pour faire cette transposition de minuscule vers majuscule, un simple décalage
de l'offset du caractère correspondant à sa position dans la table ASCII
est possible (`a=97` -> `A=65`, `b=98` -> `B=66`,  [...], `z=122` -> `Z=90`).

Le sous-programme correspondant est simple et ne fait qu'appliquer les différentes
directives énoncées précédemment&nbsp;:

```asm
.386
.model flat,stdcall
option casemap:none

include D:\bin\masm32\include\windows.inc
include D:\bin\masm32\include\kernel32.inc
include D:\bin\masm32\include\msvcrt.inc

includelib D:\bin\masm32\lib\kernel32.lib
includelib D:\bin\masm32\lib\msvcrt.lib

.DATA
    userText          db "texte en minuscule", 0
    printFormat       db "Text in uppercase: %s", 10, 0

.CODE
    uppercase PROC
        ; création d'une nouvelle stack frame
        push ebp          ; place le bas de pile (ebp) en haut de pile (esp)
        mov ebp, esp      ; place la valeur de esp dans ebp

        mov esi, [esp+8]  ; récupère la chaîne de caractères à traiter
                          ; depuis la stack et la place dans le registre esi

        for_loop:
            mov al, [esi] ; récupère un caractère de la chaîne et le place dans al

            cmp al, 97    ; compare la lettre à 'a'
            jb next       ; si la lettre est inférieure à 97 (below), on passe au suivant

            cmp al, 122   ; compare la lettre à 'z'
            ja next       ; si la lettre est supérieure à 122 (above), on passe au suivant

            sub al, 32    ; si tout est bon, on soustrait 32 à la valeur décimale du caractère (passage en majuscule)
            mov [esi], al ; place al dans esi (mise à jour du caractère minuscule)

        next:
            inc esi       ; incrémente notre position dans le registre ESI (passage au caractère suivant)
            test al, al   ; vérifie qu'il reste un caractère à traiter
            jnz for_loop  ; si il reste un caractère, on retourne dans la boucle pour le passer en majuscule

        return:
            mov esp, ebp  ; retour sur l'ancienne stackframe
            pop ebp       ; place la valeur contenue dans ebp sur la stack (adresse de retour)
            ret           ; retour à l'adresse indiquée sur la stack (précédemment dans ebp)
    uppercase ENDP

    start:
        push offset userText
        call uppercase ; appel de la fonction uppercase avec la saisie de l'utilisateur en paramètre

        push offset userText
        push offset printFormat
        call crt_printf ; affiche la saisie utilisateur en majuscule
                        ; équivalent en C : printf("Text in uppercase: %s", userText);

        mov eax, 0
        invoke ExitProcess, eax ; met fin à l'exécution du programme
                                ; et retourne un code d'erreur à 0
                                ; équivalent en C : exit(0);
    end start
```

Comme on le voit ici, on va passer l'adresse de notre variable `userText` à la fonction
`uppercase` en utilisant la *stack*.

Au sein de la fonction, après avoir initié une nouvelle *stackframe*, la valeur passée
en paramètre est disponible sur la *stack* à l'offset 8 de la stack (`esp+8`).

Une fois l'ensemble des instructions de la fonction exécuté, il ne reste plus qu'à
replacer notre haut de pile (`ebp`) sur le bas de pile (`ebp`) afin de retourner à
l'état initial de notre pile (*stackframe*). L'adresse de retour est ensuite
utilisée avec l'instruction `ret` afin de continuer l'exécution du programme principal
(ou autre sous-programme !) où il était avant l'appel de la fonction (*control flow*).

Le contenu de la fonction est suffisamment détaillé dans les commentaires, inutile d'épiloguer...

## Variables locales

Comme on peut le voir précédemment, il est possible d'utiliser la stack pour
passer des paramètres aux fonctions que l'ont souhaite appeler. Il est également
possible d'optimiser les calculs et la gestion mémoire de nos fonctions en utilisant
la stack pour stocker nos données variables.

Pour l'exemple, nous allons implémenter le code suivant en utilisant la stack pour
créer les variables locales à la fonction `myst`&nbsp;:

```c
#include <stdio.h>

int myst(int n) {
  int i, j, k, l;
  j = 1;
  k = 1;
  for (i=3; i<=n; i++) {
    l = j + k;
    j = k;
    k = l;
  }
  return k;
}

int main(int argc, char *argv[]) {
  int nValue, mystResult;

  printf("Your n value #> ");
  scanf("%d", &nValue);
  mystResult = myst(nValue);
  printf("myst function result: %d\n", mystResult);

  return 0;
}
```

L'implémentation de la fonction `myst` de base ne consiste qu'en
un jeu de tests et de branchements conditionnels élémentaires, en revanche
l'implémentation des variables locales `i`, `j`, `k` et `l` exploitant la stack
peut s'avérer complexe de prime abord.

Afin d'utiliser la *stack* pour y stocker les données variables, il est nécessaire de
déterminer la taille des données à stocker afin de pouvoir la libérer préalablement.
Par exemple, pour stocker nos 4 entiers, nous avons besoin de libérer 16 octets sur la stack,
ce qui permet d'obtenir une réprésentation de la *stackframe* telle que&nbsp;:

![stack](illustrations/stack.png)

Le code équivalent à notre algorithme en C est donc le suivant&nbsp;:

```asm
.386
.model flat,stdcall
option casemap:none

include D:\bin\masm32\include\windows.inc
include D:\bin\masm32\include\kernel32.inc
include D:\bin\masm32\include\msvcrt.inc

includelib D:\bin\masm32\lib\kernel32.lib
includelib D:\bin\masm32\lib\msvcrt.lib

.DATA
    userInputPrompt   db "Your n value #> ", 0
    mystPrompt        db "myst function result: %d", 10, 0
    scanfFormat       db "%d", 0

.DATA?
    nValue dword ?

.CODE
    myst PROC ; Suite de Fibonacci
        ; création d'une nouvelle stack frame
        push ebp          ; place le bas de pile (ebp) en haut de pile (esp)
        mov ebp, esp      ; place la valeur de esp dans ebp

        sub esp, 16       ; prépare de la place sur la stack pour stocker 4 entiers
                          ; de 32 bits (DWORD)

        mov ebx, 3
        mov [ebp-4], ebx   ; location of variable i
        mov ebx, 1
        mov [ebp-8], ebx   ; location of variable j
        mov ebx, 1
        mov [ebp-12], ebx  ; location of variable k
        xor ebx, ebx
        mov [ebp-16], ebx  ; location of variable l

        mov ecx, [ebp-4]   ; move i to ecx
        ; for (i=3; i <= n; i++)
        for_loop:
            ; l = j + k
            mov ebx, [ebp-8]
            add ebx, [ebp-12]
            mov [ebp-16], ebx

            ; j = k
            mov ebx, [ebp-12]
            mov [ebp-8], ebx

            ; k = l
            mov ebx, [ebp-16]
            mov [ebp-12], ebx

            ; i++
            ; mov ebx, [ebp-4]
            ; inc ebx
            ; mov [ebp-4], ebx
            inc ecx

            ; compare i à n et fait un branchement vers la boucle si i > n
            mov eax, [ebp+8]
            cmp ecx, eax
            jle for_loop

        return:
            mov eax, [ebp-12] ; retourne k
            mov esp, ebp      ; retour sur l'ancienne stackframe
            pop ebp           ; place la valeur contenue dans ebp sur la stack (adresse de retour)
            ret               ; retour à l'adresse indiquée sur la stack (précédemment dans ebp)
    myst ENDP

    start:
        push offset userInputPrompt
        call crt_printf ; Affiche le prompt pour la saisie utilisateur
                        ; équivalent en C : printf("Your n value #> ");

        push offset nValue
        push offset scanfFormat
        call crt_scanf ; récupère la saisie utilisateur depuis l'entrée standard
                       ; et la stocke dans nValue
                       ; équivalent en C : scanf("%s", &nValue);

        push dword ptr nValue
        call myst ; appel de la fonction uppercase avec la saisie de l'utilisateur en paramètre

        push eax
        push offset mystPrompt
        call crt_printf ; affiche la saisie utilisateur

        mov eax, 0
        invoke ExitProcess, eax ; met fin à l'exécution du programme
                                ; et retourne un code d'erreur à 0
                                ; équivalent en C : exit(0);
    end start
```

Après quelques tests, on constate rapidement que la fonction `myst` consiste
à calculer la nième valeur dans la [suite de Fibonacci](https://en.wikipedia.org/wiki/Fibonacci_sequence).

## Syscalls Windows

En pratique deux contextes ou modes d'exécution permettent l'exécution
d'instructions sous Windows, le *userland* (ou contexte utilisateur, *ring 3*) et le
*kernel space* (ou mode noyau, *ring 0*).

Cette séparation est souvent orchestrée au moyen d'une coopération entre une
gestion matérielle et d'une gestion logicielle au niveau du système d'exploitation.

Cette isolation est souvent symbolisée sous la forme d'anneaux de privilèges
(ou *protections rings*) allants de 0 à 3, le *ring 3* (*kernel space*) étant
le contexte disposant du plus de privilèges et le *ring 0* (*userland*) disposant
de privilèges restreints&nbsp;:

![windows_rings](illustrations/windows_rings.svg)

Le niveau de privilège couramment (*&laquo;&nbsp;Current Privilege Level&nbsp;&raquo;*
ou *CPL*) exploité par un programme est décrit dans les registres `CS` et `SS`.

Ce *CPL* définit une limite directe pour l'exécution d'instructions privilégiées&nbsp;:
il n'est pas possible de modifier directement cette valeur dans les registres
afin de disposer d'avantages de privilèges au cours de l'exécution d'un programme.

Afin d'effectuer des appels système (*syscall*), il existe une procédure
appelée `KiFastSystemCall` permettant de réaliser cette transition en mode noyau
en utilisant l'instruction `sysenter`.

L'instruction `sysenter` conjointe à l'instruction `sysexit` permet
d'exécuter un appel système particulier, décrit dans le registre MSR en mode noyau
et retourner dans le `userland`.

 [debugger_hello_world]: illustrations/debugger_hello_world.png
 [debugger_hello_world_call_printf]: illustrations/debugger_hello_world_call_printf.png
