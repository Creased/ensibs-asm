.386
.model flat,stdcall
option casemap:none

include D:\bin\masm32\include\windows.inc
include D:\bin\masm32\include\kernel32.inc
include D:\bin\masm32\include\msvcrt.inc

includelib D:\bin\masm32\lib\kernel32.lib
includelib D:\bin\masm32\lib\msvcrt.lib

.DATA
    orginalTextPrompt db "Your original text: %s", 10, 0
    upperCasePrompt   db "Your text in uppercase: %s", 10, 0
    userInputPrompt   db "Your text #> ", 0
    scanfFormat       db "%s", 0

.DATA?
    userText db ?

.CODE
    uppercase PROC
        ; création d'une nouvelle stack frame
        push ebp          ; place le bas de pile (ebp) en haut de pile (esp)
        mov ebp, esp      ; place la valeur de esp dans ebp

        mov esi, [esp+8]  ; récupère la chaîne de caractères à traiter
                          ; depuis la stack et la place dans le registre esi

        for_loop:
            mov al, [esi] ; récupère un caractère de la chaîne et le place dans al

            cmp al, 97    ; compare la lettre à 'a'
            jb next       ; si la lettre est inférieure à 97 (below), on passe au suivant

            cmp al, 122   ; compare la lettre à 'z'
            ja next       ; si la lettre est supérieure à 122 (above), on passe au suivant

            sub al, 32    ; si tout est bon, on soustrait 32 à la valeur décimale du caractère (passage en majuscule)
            mov [esi], al ; place al dans esi (mise à jour du caractère minuscule)

        next:
            inc esi       ; incrémente notre position dans le registre ESI (passage au caractère suivant)
            test al, al   ; vérifie qu'il reste un caractère à traiter
            jnz for_loop  ; si il reste un caractère, on retourne dans la boucle pour le passer en majuscule

        return:
            mov esp, ebp  ; retour sur l'ancienne stackframe
            pop ebp       ; place la valeur contenue dans ebp sur la stack (adresse de retour)
            ret           ; retour à l'adresse indiquée sur la stack (précédemment dans ebp)
    uppercase ENDP

    start:
        push offset userInputPrompt
        call crt_printf ; Affiche le prompt pour la saisie utilisateur
                        ; équivalent en C : printf("Your text #> ");

        push offset userText
        push offset scanfFormat
        call crt_scanf ; récupère la saisie utilisateur depuis l'entrée standard
                       ; et la stocke dans userText
                       ; équivalent en C : scanf("%s", &userText);

        push offset userText
        push offset orginalTextPrompt
        call crt_printf ; affiche la saisie utilisateur
                        ; équivalent en C : printf("Your original text: %s\n", userText);

        push offset userText
        call uppercase ; appel de la fonction uppercase avec la saisie de l'utilisateur en paramètre

        push offset userText
        push offset upperCasePrompt
        call crt_printf ; affiche la saisie utilisateur en majuscule
                        ; équivalent en C : printf("Your text in uppercase: %s", userText);

        mov eax, 0
        invoke ExitProcess, eax ; met fin à l'exécution du programme
                                ; et retourne un code d'erreur à 0
                                ; équivalent en C : exit(0);
    end start
